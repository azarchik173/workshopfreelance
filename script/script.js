document.addEventListener("DOMContentLoaded", () => {
  "use strict";

  const customer = document.getElementById("customer");
  const freelancer = document.getElementById("freelancer");
  const blockCustomer = document.getElementById("block-customer");
  const blockFreelancer = document.getElementById("block-freelancer");
  const blockChoice = document.getElementById("block-choice");
  const buttonExit = document.getElementById("btn-exit");
  const formCustomer = document.getElementById("form-customer");
  const orderTable = document.getElementById("orders");
  const modalOrder = document.getElementById("order_read");
  const modalOrderActive = document.getElementById("order_active");

  const orders = [];

  const renderOrder = () => {
    orderTable.innerHTML = "";
    orders.forEach((order, index) => {
      orderTable.innerHTML += `
        <tr class="order" data-number-order="${index}">
            <td>${index + 1}</td>
            <td>${order.title}</td>
            <td class="${order.currency}"></td>
            <td>${order.deadline}</td>
        </tr>`;
    });
  };
  const openModal = numberOrder => {
    const order = orders[numberOrder];
    const modal = order.active ? modalOrderActive : modalOrder;
    modal.style.display = "block";

    const modalTitleBlock = document.querySelector(".modal-title"),
      closeModalButton = document.querySelector(".close"),
      firstNameBlock = document.querySelector(".firstName"),
      mailBlock = document.querySelector(".email"),
      descriptionBlock = document.querySelector(".description"),
      deadLineBlock = document.querySelector(".deadline"),
      currencyBlock = document.querySelector(".currency_img"),
      countBlock = document.querySelector(".count"),
      phoneBlock = document.querySelector(".phone");

    modalTitleBlock.textContent = order.title;
    firstNameBlock.textContent = order.firstName;
    mailBlock.textContent = order.email;
    descriptionBlock.textContent = order.description;
    deadLineBlock.textContent = order.deadline;
    countBlock.textContent = order.amount;
    currencyBlock.className = order.currency;
    phoneBlock.href = order.phone;

    closeModalButton.addEventListener("click", () => {
      modal.style.display = "none";
    });
  };

  orderTable.addEventListener("click", e => {
    const orderTarget = e.target.closest(".order");
    orderTarget && openModal(orderTarget.dataset.numberOrder);
  });

  customer.addEventListener("click", () => {
    blockChoice.style.display = "none";
    blockCustomer.style.display = "block";
    buttonExit.style.display = "block";
  });
  freelancer.addEventListener("click", () => {
    blockChoice.style.display = "none";
    renderOrder();
    blockFreelancer.style.display = "block";
    buttonExit.style.display = "block";
  });
  buttonExit.addEventListener("click", () => {
    blockFreelancer.style.display = "none";
    blockCustomer.style.display = "none";
    buttonExit.style.display = "none";
    blockChoice.style.display = "block";
  });
  formCustomer.addEventListener("submit", e => {
    e.preventDefault();
    const obj = {};
    const elements = [...formCustomer.elements].filter(
      elem =>
        (elem.tagName === "INPUT" && elem.type !== "radio") ||
        (elem.type === "radio" && elem.checked) ||
        elem.tagName === "TEXTAREA"
    );
    elements.forEach(elem => {
      obj[elem.name] = elem.value;
      // elem.type !== 'radio' && (elem.value = '')
    });
    orders.push(obj);
    formCustomer.reset();
  });
});
